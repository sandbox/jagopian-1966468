   _____           _     _   ______           _              _   _       
  / ____|         | |   | | |  ____|         | |            | | | |      
 | (___   ___ __ _| | __| | | |__   _ __ ___ | |__   ___  __| | | |_   _ 
  \___ \ / __/ _` | |/ _` | |  __| | '_ ` _ \| '_ \ / _ \/ _` | | | | | |
  ____) | (_| (_| | | (_| | | |____| | | | | | |_) |  __/ (_| |_| | |_| |
 |_____/ \___\__,_|_|\__,_| |______|_| |_| |_|_.__/ \___|\__,_(_)_|\__, |
                        ______                                      __/ |
                       |______|                                    |___/ 

Overview
--------
Scald_Embed.ly is a provider allowing Scald module users to 
embed any type of media atoms supported by embed.ly. 

Embed supports more than 250 content providers. Video, audio, 
photos, products, and more—embed the content your users crave. 

No need to modify code if providers change the way they deliver content; 
the Embedly team does it for you.


Requirements
------------
  - Drupal 7.x,
  - PHP 5.2 or greater,
  - Scald 1.x


Installing
----------
See http://drupal.org/getting-started/install-contrib for instructions
on how to install or update Drupal modules.


Credits
-------
Scald embed.ly is developed by Open Web Solutions.
